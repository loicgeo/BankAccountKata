package com.arolla.kata.accountbank.cucumber.steps;

import com.arolla.kata.accountbank.domain.account.AccountImpl;
import com.arolla.kata.accountbank.domain.account.AccountService;
import com.arolla.kata.accountbank.domain.account.Client;
import com.arolla.kata.accountbank.domain.account.ClientImpl;
import com.arolla.kata.accountbank.domain.transaction.Currency;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class WithdrawSteps {

    private AccountService accountService;

    @Given("^an existing client named \"(.*)\" with (\\d+\\.\\d+) (.*) in his account$")
    public void init_account(String clientName, String initialMoney, Currency currency) throws Throwable {
        Client client = new ClientImpl(clientName);
        accountService = new AccountImpl(client, new BigDecimal(initialMoney), currency);
    }

    @When("^he withdraws (\\d+\\.\\d+) (.*) from his account$")
    public void withdraw_transaction(String moneyWithdraw, Currency currency) throws Throwable {
        accountService.withdraw(new BigDecimal(moneyWithdraw), currency);
    }

    @Then("^the new balance is (\\d+\\.\\d+) (.*)$")
    public void verify_transactions(String balanceToVerify, Currency currency) throws Throwable {
        assertThat(accountService.getBalance().getAmount()).isEqualTo(new BigDecimal(balanceToVerify));
        assertThat(accountService.getCurrency()).isEqualTo(currency);
    }
}
