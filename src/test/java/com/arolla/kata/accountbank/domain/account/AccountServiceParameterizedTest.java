package com.arolla.kata.accountbank.domain.account;


import com.arolla.kata.accountbank.exceptions.business.BusinessException;
import com.arolla.kata.accountbank.exceptions.business.NotEnoughMoneyException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;

import static com.arolla.kata.accountbank.domain.transaction.Currency.EUR;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AccountServiceParameterizedTest {
    private AccountService accountService;

    @BeforeEach
    void initialise_account_with_100_EUR() {
        // Given
        accountService = new AccountImpl(new ClientImpl("Jean-Pierre"), new BigDecimal(100), EUR);
    }

    @ParameterizedTest
    @CsvSource({
            "10.0, 90.0",
            "30.5, 69.5"
    })
    void withdraw_x_EUR_on_account_with_100_EUR(double withdrawedAmount, double newBalance) throws BusinessException {
        // When
        accountService.withdraw(new BigDecimal(withdrawedAmount), EUR);
        // Then
        assertThat(accountService.getBalance().getAmount().doubleValue()).isEqualTo(newBalance);
        assertThat(accountService.getCurrency()).isEqualTo(EUR);
    }

    @Test
    void withdraw_110_EUR_on_account_with_100_EUR() throws BusinessException {
        assertThrows(
                // Then
                NotEnoughMoneyException.class, () -> {
                    // When
                    accountService.withdraw(new BigDecimal(110), EUR);
                });
    }

}
