package com.arolla.kata.accountbank.domain.account;


import com.arolla.kata.accountbank.exceptions.business.BusinessException;
import com.arolla.kata.accountbank.exceptions.business.NotEnoughMoneyException;
import com.arolla.kata.accountbank.exceptions.business.WrongCurrencyException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.arolla.kata.accountbank.domain.transaction.Currency.EUR;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


class AccountServiceTest {
    private AccountService accountService;

    @BeforeEach
    void initialise_account_with_100_EUR() {
        // Given
        accountService = new AccountImpl(new ClientImpl("Jean-Pierre"), new BigDecimal(100), EUR);
    }

    @Test
    void withdraw_10_EUR_on_account_with_100_EUR() throws BusinessException {
        // When
        accountService.withdraw(new BigDecimal(10), EUR);
        // Then
        assertThat(accountService.getBalance().getAmount().intValue()).isEqualTo(90);
        assertThat(accountService.getCurrency()).isEqualTo(EUR);
    }

    @Test
    void withdraw_110_EUR_on_account_with_100_EUR() throws BusinessException {
        // Then
        assertThrows(NotEnoughMoneyException.class,
                // When
                () -> accountService.withdraw(new BigDecimal(110), EUR));
    }

    @Test
    void withdraw_wrong_currency_on_account_EUR() throws BusinessException {
        // Then
        assertThrows(WrongCurrencyException.class,
                // When
                () -> accountService.withdraw(new BigDecimal(10), null));
    }
}
