package com.arolla.kata.accountbank.domain.account;

import com.arolla.kata.accountbank.domain.transaction.Currency;
import com.arolla.kata.accountbank.exceptions.business.BusinessException;

import java.math.BigDecimal;

public interface AccountService {


    /**
     * Perform a withdraw transaction on account.
     *
     * @param amount   the amount to withdraw
     * @param currency the currency for the transaction
     * @throws BusinessException
     */
    void withdraw(BigDecimal amount, Currency currency) throws BusinessException;

    /**
     * Return the current balance on account.
     *
     * @return current amount on this account // TODO need of currency in wrapped object?
     * @throws BusinessException
     */
    Money getBalance() throws BusinessException;

    /**
     * Return currency used on account.
     *
     * @return currency on this account // TODO need to merge concept in a wrapped Balance object?
     */
    Currency getCurrency();
}
