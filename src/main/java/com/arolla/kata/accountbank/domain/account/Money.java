package com.arolla.kata.accountbank.domain.account;

import com.arolla.kata.accountbank.domain.transaction.Currency;

import java.math.BigDecimal;

public class Money {

    private final Currency currency;
    private final BigDecimal amount;

    public Money(BigDecimal amount, Currency currency) {
        this.currency = currency;
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }
}
