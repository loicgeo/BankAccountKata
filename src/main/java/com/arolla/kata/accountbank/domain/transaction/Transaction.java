package com.arolla.kata.accountbank.domain.transaction;

import java.math.BigDecimal;
import java.time.LocalDate;

public abstract class Transaction {
    private final LocalDate date;
    protected final BigDecimal amount;

    public Transaction(BigDecimal amount) {
        this.date = LocalDate.now();
        this.amount = amount;
    }

    /**
     * Compute transaction on balance amount to return new balance amount.
     *
     * @param balance amount before calculation
     * @return the new balance applying transaction
     */
    public abstract BigDecimal applyOn(BigDecimal balance);

    public LocalDate getDate() {
        return date;
    }

}
