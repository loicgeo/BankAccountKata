package com.arolla.kata.accountbank.domain.account;

import com.arolla.kata.accountbank.domain.transaction.Currency;
import com.arolla.kata.accountbank.domain.transaction.Transaction;
import com.arolla.kata.accountbank.domain.transaction.WithdrawTransaction;
import com.arolla.kata.accountbank.exceptions.business.BusinessException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

public abstract class Account implements AccountService {
    private final Client client;
    protected final Money initialBalance;
    protected final Collection<Transaction> transactions;

    public Account(Client client, BigDecimal initialBalance, Currency currency) {
        this.client = client;
        this.initialBalance = new Money(initialBalance, currency);
        this.transactions = new ArrayList<>();
    }

    public synchronized void withdraw(BigDecimal amount, Currency currency) throws BusinessException {
        transactions.add(new WithdrawTransaction(amount));
    }

    public Money getBalance() throws BusinessException {
        BigDecimal balance = initialBalance.getAmount();
        for (Transaction tr : transactions) {
            balance = tr.applyOn(balance);
        }
        return new Money(balance, initialBalance.getCurrency());
    }

    @Override
    public Currency getCurrency() {
        return initialBalance.getCurrency();
    }

}
