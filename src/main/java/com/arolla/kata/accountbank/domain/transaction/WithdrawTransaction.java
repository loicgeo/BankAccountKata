package com.arolla.kata.accountbank.domain.transaction;

import java.math.BigDecimal;
import java.math.MathContext;

public class WithdrawTransaction extends Transaction {

    public WithdrawTransaction(BigDecimal amount) {
        super(amount);
    }

    public BigDecimal applyOn(BigDecimal balance) {
        return balance.subtract(amount, MathContext.UNLIMITED);
    }
}
