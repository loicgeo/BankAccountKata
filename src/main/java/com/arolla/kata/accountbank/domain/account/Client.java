package com.arolla.kata.accountbank.domain.account;

public abstract class Client {

    private final String name;

    public Client(String name) {
        this.name = name;
    }

}
