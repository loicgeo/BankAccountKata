package com.arolla.kata.accountbank.domain.account;

import com.arolla.kata.accountbank.domain.transaction.Currency;
import com.arolla.kata.accountbank.exceptions.business.BusinessException;
import com.arolla.kata.accountbank.exceptions.business.NotEnoughMoneyException;
import com.arolla.kata.accountbank.exceptions.business.WrongCurrencyException;

import java.math.BigDecimal;
import java.math.MathContext;

public class AccountImpl extends Account {
    public AccountImpl(Client client, BigDecimal initialAmount, Currency currency) {
        super(client, initialAmount, currency);
    }

    public synchronized void withdraw(BigDecimal amount, Currency currency) throws BusinessException {
        checkCurrency(currency);
        checkEnoughMoney(amount);
        super.withdraw(amount, currency);
    }

    private void checkEnoughMoney(BigDecimal amount) throws BusinessException {
        if (getBalance().getAmount().subtract(amount, MathContext.UNLIMITED).signum() == -1) {
            throw new NotEnoughMoneyException();
        }
    }

    private void checkCurrency(Currency currency) throws WrongCurrencyException {
        if (!this.initialBalance.getCurrency().equals(currency)) {
            throw new WrongCurrencyException();
        }
    }
}
