package com.arolla.kata.accountbank.exceptions.business;

public abstract class BusinessException extends Throwable {
    BusinessException() {
        super();
    }

    BusinessException(String message) {
        super(message);
    }
}
